# Request ID Express Middleware

We use request tracing to track http requests from clients to targets or other services. This package associates a passed requestId with the `req` object or otherwise creates one. It also adds the service under a defined name to a trace header which will also be made available via the `req` object. By default the trace header will be added to the response as well.

The package defaults to the request and trace headers used by amazon web services. You can change these settings during initialization.

## Migration from 1.x to 2.x
Use
```javascript
const { requestId } = require('@openreply/emw-request-id');
```
instead of
```javascript
const requestId = require('@openreply/emw-request-id');
```
for the import.

## Getting Started

### Installing

you can install this package via npm

`npm i @openreply/emw-request-id`

### Usage

Simply add this module to your middleware chain

```javascript
app.use(requestId({ serviceName: 'testService' }));
```

See the [API documentation](API.md) for more information regarding the initialization options.

the middleware will extend the request object by the following properties

| Param | Type | Description |
| --- | --- | --- |
| req.trace | string | a list of all nodes that appended to the trace in order newest to oldest in '[key]=[value];...' format | string | in case a requestId has been passed with the request will be that headers value. |
| req.traceHeader | string | the name of the header that will be used to receive and pass on the trace id (defaults to `X-Amzn-Trace-Id`) |
| req.requestId | string | the contents of the `x-amzn-RequestId` header if passed with the request. Otherwise a uuid/v4. The requestId header and id generator can be customized during initialization |


See the following usage example

```javascript
/* eslint-disable import/no-extraneous-dependencies */
const request = require('request');
const express = require('express');
/* eslint-enable import/no-extraneous-dependencies */
const { requestId } = require('@openreply/emw-request-id');

const app = express();

// initializes our request-id middleware. will also set the trace header on
// the response object by default
app.use(requestId({ serviceName: 'testService' }));

app.get('/', (req, res) => {
  // log the trace and request information to the console
  // eslint-disable-next-line no-console
  console.log(`incoming request with requestId ${req.requestId} and trace ${req.trace} for trace header ${req.traceHeader}`);

  // add the trace information to requests to upstream servers
  request.get({
    url: 'http://www.example.com',
    headers: {
      [req.traceHeader]: req.trace
    }
  }, (error, response, body) => {
    if (error) {
      return res.sendStatus(502);
    }
    return res.send(body);
  });
});

app.listen(3000, () => {
  // eslint-disable-next-line no-console
  console.log('Example app listening on port 3000!');
});

```

## Running the tests

You can run the test suite via

`npm test`

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/openreply-de/emw-request-id). 

## Authors

* **Florian Schaper** - *Initial work* - [openreply-de](https://bitbucket.org/openreply-de/emw-request-id)

See also the list of [authors](AUTHORS.md) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## References

- AWS Application Load Balancers enrich incoming http requests with an additional `X-Amzn-Trace-Id` header - see the [aws documentation](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-request-tracing.html) for more information.

- AWS API Gateway sets a `x-amzn-RequestId` header with an unique id for every incoming request - have a look at [their documentation](https://docs.aws.amazon.com/apigateway/api-reference/making-http-requests/) for more information.
