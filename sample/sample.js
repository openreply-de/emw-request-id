/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved */
const request = require('request');
const express = require('express');
/* eslint-enable import/no-extraneous-dependencies, import/no-unresolved */
const { requestId } = require('..');

const app = express();

// initializes our request-id middleware. will also set the trace header on
// the response object by default
app.use(requestId({ serviceName: 'testService' }));

app.get('/', (req, res) => {
  // log the trace and request information to the console
  // eslint-disable-next-line no-console
  console.log(`incoming request with requestId ${req.requestId} and trace ${req.trace} for trace header ${req.traceHeader}`);

  // add the trace information to requests to upstream servers
  request.get({
    url: 'http://www.example.com',
    headers: {
      [req.traceHeader]: req.trace
    }
  }, (error, response, body) => {
    if (error) {
      return res.sendStatus(502);
    }
    return res.send(body);
  });
});

app.listen(3000, () => {
  // eslint-disable-next-line no-console
  console.log('Example app listening on port 3000!');
});
