import { IncomingMessage, ServerResponse } from "http";

declare interface ExpressMiddleware {
  (req: IncomingMessage, res: ServerResponse, next?: () => void): void;
}
declare interface UuidGenerator {
  (): string;
}
declare interface Options {
  serviceName: string;
  traceHeader?: string;
  requestIdHeader?: string;
  setResponseHeader?: boolean;
  uuidGenerator?: UuidGenerator;
}
export function requestId(options: Options): ExpressMiddleware;
