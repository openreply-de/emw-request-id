
## requestId(options) ⇒ RequestMiddleware
searches for a trace id in the request headers or otherwise creates one

we use a trace id for requests in order to associate multiple e.g. log-entries or cross service calls
to an specific incoming request

**Kind**: global function  
**Throws**:

- TypeError in case that the middleware has been passed incorrectly to express
- TypeError if not passed an options object
- TypeError if not given a serviceName with the options


| Param | Type | Description |
| --- | --- | --- |
| options | RequestIdConfiguration | configuration options for this middleware |

## UUIDGenerator : function
returns a unique id for every call

**Kind**: global typedef  
## RequestIdConfiguration : Object
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| serviceName | string | name of the service that will be appended to the trace id string |
| traceHeader | string | the request header to take the trace id from. defaults to `X-Amzn-Trace-Id`. |
| requestIdHeader | string | the request header to take the request id from. defaults to `x-amzn-RequestId`. |
| setResponseHeader | boolean | this will add the current `options.traceHeader` key and value to the headers of the response object. defaults to true. |
| uuidGenerator | UUIDGenerator | function that will return an unique id. by default v4 uuids will be used. |

## RequestMiddleware : function
**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| req | Object | the express request object (see http://expressjs.com/en/4x/api.html#req) |
| res | Object | the express response object (see http://expressjs.com/en/4x/api.html#res) |
| next | Object | the next middleware object to be executed from the express middleware list |

