const { expect } = require('chai');
const sinon = require('sinon');
const { requestId } = require('..');

describe('emw-request-id', () => {
  it('should throw if passed purely as a reference into the middleware chain', () => {
    expect(() => requestId({}, {}, () => {})).to.throw(TypeError);
  });

  it('should require an options object otherwise throw', () => {
    expect(() => requestId()).to.throw(/expects to be passed an options object/);
  });

  it('should require an service name and otherwise throw', () => {
    expect(() => requestId({})).to.throw(/you need to pass a serviceName/);
  });

  it('should export a function', () => {
    expect(requestId({ serviceName: 'test' })).to.be.a('function');
  });

  it('should call the next middleware after executing', () => {
    const next = sinon.spy();
    requestId({ serviceName: 'test' })({ get: () => {} }, { set: () => {} }, next);
    expect(next.calledOnce).to.be.true;
  });

  it('should create a new requestId if none was passed via the request header', () => {
    const req = {
      get: sinon.fake.returns(undefined)
    };
    const res = {
      set: sinon.spy()
    };

    requestId({ serviceName: 'test' })(req, res, () => {});

    expect(req.get.calledWith('x-amzn-RequestId')).to.be.true;
    expect(res.set.calledWith('X-Amzn-Trace-Id')).to.be.true;
    expect(res.set.args[0][1]).to.match(/test=[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/);
  });

  it('should create a different request id for every request (if none was passed via the request header)', () => {
    const createRequest = () => ({ req: { get: sinon.fake.returns(undefined) }, res: { set: sinon.spy() } });
    const req1 = createRequest();
    const req2 = createRequest();

    requestId({ serviceName: 'test' })(req1.req, req1.res, () => {});
    requestId({ serviceName: 'test' })(req2.req, req2.res, () => {});

    expect(req1.req.get.calledWith('x-amzn-RequestId')).to.be.true;
    expect(req1.res.set.calledWith('X-Amzn-Trace-Id')).to.be.true;
    expect(req1.res.set.args[0][1]).to.match(/test=[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/);

    expect(req2.req.get.calledWith('x-amzn-RequestId')).to.be.true;
    expect(req2.res.set.calledWith('X-Amzn-Trace-Id')).to.be.true;
    expect(req2.res.set.args[0][1]).to.match(/test=[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/);

    expect(req1.req.requestId).to.not.equal(req2.req.requestId);
  });

  it('should reuse an request id that has been passed with the request header', () => {
    const req = {
      get: sinon.stub()
    };
    req.get.withArgs('x-amzn-RequestId').returns('90f43a89-c274-4775-a6fe-8ce6f1af1dcd');
    const res = {
      set: sinon.spy()
    };

    requestId({ serviceName: 'test' })(req, res, () => {});

    expect(req.get.calledWith('x-amzn-RequestId')).to.be.true;
    expect(res.set.calledWith('X-Amzn-Trace-Id')).to.be.true;
    expect(res.set.args[0][1]).to.equal('test=90f43a89-c274-4775-a6fe-8ce6f1af1dcd;');
  });

  it('should append the requestId to an existing traceId', () => {
    const req = {
      get: sinon.stub()
    };
    req.get.withArgs('x-amzn-RequestId').returns('90f43a89-c274-4775-a6fe-8ce6f1af1dcd');
    req.get.withArgs('X-Amzn-Trace-Id').returns('Root=1-67891233-abcdef012345678912345678');
    const res = {
      set: sinon.spy()
    };

    requestId({ serviceName: 'test' })(req, res, () => {});

    expect(req.get.calledWith('x-amzn-RequestId')).to.be.true;
    expect(res.set.calledWith('X-Amzn-Trace-Id', 'test=90f43a89-c274-4775-a6fe-8ce6f1af1dcd;Root=1-67891233-abcdef012345678912345678')).to.be.true;
  });

  it('should set an trace string on the req object', () => {
    const req = {
      get: sinon.stub()
    };
    req.get.withArgs('x-amzn-RequestId').returns('2d5bd87c-5c13-4bf8-a6d2-f07aaf4bcaa1');
    req.get.withArgs('X-Amzn-Trace-Id').returns('Root=1-67891233-abcdef012345678912345678');
    const res = {
      set: sinon.spy(),
      send: sinon.spy()
    };

    requestId({ serviceName: 'test' })(req, res, () => {});

    expect(req.trace).to.equal('test=2d5bd87c-5c13-4bf8-a6d2-f07aaf4bcaa1;Root=1-67891233-abcdef012345678912345678');
  });

  it('should set the trace header name on the req object', () => {
    const req = {
      get: sinon.stub()
    };
    req.get.withArgs('x-amzn-RequestId').returns('2d5bd87c-5c13-4bf8-a6d2-f07aaf4bcaa1');
    req.get.withArgs('X-Amzn-Trace-Id').returns('Root=1-67891233-abcdef012345678912345678');
    const res = {
      set: sinon.spy(),
      send: sinon.spy()
    };

    requestId({ serviceName: 'test' })(req, res, () => {});

    expect(req.traceHeader).to.equal('X-Amzn-Trace-Id');
  });

  it('should forward the header requestId variable on the req object', () => {
    const req = {
      get: sinon.stub()
    };
    req.get.withArgs('x-amzn-RequestId').returns('2d5bd87c-5c13-4bf8-a6d2-f07aaf4bcaa1');

    requestId({ serviceName: 'test' })(req, { set: () => {} }, () => {});

    expect(req.requestId).to.equal('2d5bd87c-5c13-4bf8-a6d2-f07aaf4bcaa1');
  });

  it('should set a requestId variable on the req object even if no header was set', () => {
    const req = {
      get: sinon.stub()
    };

    requestId({ serviceName: 'test' })(req, { set: () => {} }, () => {});

    expect(req.requestId).to.match(/[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/);
  });

  it('should not set the response header in case setResponseHeader option is set to false', () => {
    const req = {
      get: sinon.stub()
    };
    req.get.withArgs('x-amzn-RequestId').returns('2d5bd87c-5c13-4bf8-a6d2-f07aaf4bcaa1');
    req.get.withArgs('X-Amzn-Trace-Id').returns('Root=1-67891233-abcdef012345678912345678');
    const res = {
      set: sinon.spy()
    };
    requestId({ serviceName: 'test', setResponseHeader: false })(req, res, () => {});

    expect(res.set.calledWith('X-Amzn-Trace-Id')).to.be.false;
  });
});
